import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { _ } from 'lodash'
import Dragula from 'react-dragula'
import { withRouter } from 'react-router-dom'

import 'dragula/dist/dragula.css'

import './styles.less'

import { formatTime } from '../../../lib/time'

import Step from '../Step/index'

import { addStep } from '../../../redux/actions'

// function mapStateToProps(state) {  
//   return { ...state }
// }

function mapDispatchToProps(dispatch) {  
  return {
    addStep: newStep => dispatch(addStep(newStep)),
  }
}

class Stage extends Component {
  static propTypes = {
    stage: PropTypes.shape({
      index: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      steps: PropTypes.array.isRequired,
    }).isRequired,
  }

  static defaultProps = {
    // propName: 'value',
  }

  constructor() {
    super()

    this.stepsContainer = React.createRef()

    this.state = {
      expand: false,
    }
  }

  componentDidMount() {
    Dragula([this.stepsContainer.current], {
      moves(el, container, target) {
        const parent = target.closest('[data-role="dragula/container"]')
        return parent.dataset.id === 'steps'
      }
    })
  }

  expandStage() {
    const { expand } = this.state
    this.setState({
      expand: !expand,
    })
  }

  addStep() {
    const { stage, addStep } = this.props
    const { steps } = stage

    let maxIndexStep = _.maxBy(steps, 'index')
    let maxIndex

    if (_.isUndefined(maxIndexStep)) {
      maxIndex = 0
    } else {
      maxIndex = maxIndexStep.index
    }

    const index = maxIndex + 1

    addStep({
      stageIndex: stage.index,
      content: {
        title: `Шаг №${ index }`,
        index,
        elements: [],
      }
    })
    this.setState({
      expand: true,
    })
  }

  render() {
    const { expand } = this.state
    const { stage, history } = this.props
    const { steps } = stage

    let time = 0

    for (const step of steps) {
      for (const element of step.elements) {
        const timeArr = element.time.split(':')
        time += +timeArr[0] * 60 + +timeArr[1]
      }  
    }

    return (
      <div className="row stage__row">
        <div className="col stage__col">
          <div className="stage__title-cont">
            <div>
              <div onClick={ () => history.push(`/${ stage.index }`) }>{ stage.title }</div>
              <div>{ formatTime(time) }</div>
            </div>
            <div>
              <a href="javascript:" className="stage__expand-btn" onClick={ this.expandStage.bind(this) }>{ expand ? 'Свернуть' : 'Развернуть' }</a>
              <button className="btn btn-primary" onClick={ this.addStep.bind(this) }>+</button>
            </div>
          </div>
          <div className="stage__steps" data-role="dragula/container" data-id="steps" ref={ this.stepsContainer }>
            { expand && steps.map(step => (
              <Step key={ step.index } step={ step } stageIndex={ stage.index } />
            )) }
          </div>
        </div>
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(withRouter(Stage))
