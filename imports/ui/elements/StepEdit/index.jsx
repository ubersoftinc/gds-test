import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { _ } from 'lodash'

import './styles.less'
import '../../styles/panel.less'

import { changeStep } from '../../../redux/actions'

function mapStateToProps(state) {  
  return { ...state.stages }
}

function mapDispatchToProps(dispatch) {  
  return {
    changeStep: newProps => dispatch(changeStep(newProps)),
  }
}

class StepEdit extends Component {
  constructor() {
    super()

    this.state = {
      title: null,
    }

    this.getStep = this.getStep.bind(this)
    this.titleInput = React.createRef()
  }

  changeTitle(event) {
    this.setState({
      title: event.target.value,
    })
  }

  saveChanges() {
    const { title } = this.state
    const { history } = this.props

    if (!_.isNull(title)) {
      const { match, changeStep } = this.props
      const stageIndex = +match.params.stageIndex
      const stepIndex = +match.params.stepIndex

      changeStep({
        stageIndex,
        stepIndex,
        content: {
          title,
        },
      })
    }
    
    history.push('/')
  }

  componentDidUpdate(prevProps) {
    const { stages, match } = this.props
    const { match: prevMatch } = prevProps

    const stageIndex = +match.params.stageIndex
    const stepIndex = +match.params.stepIndex

    if (+prevMatch.params.stageIndex !== stageIndex || +prevMatch.params.stepIndex !== stepIndex) {
      const step = this.getStep()

      if (_.isUndefined(step)) {
        return
      }
  
      this.titleInput.current.value = step.title
    }
  }

  getStep() {
    const { stages, match } = this.props
    const stageIndex = +match.params.stageIndex
    const stepIndex = +match.params.stepIndex

    const stage = _.find(stages, ['index', stageIndex])

    if (_.isUndefined(stage)) {
      return
    }

    return _.find(stage.steps, ['index', stepIndex])
  }

  render() {
    const { history } = this.props

    const step = this.getStep()

    if (_.isUndefined(step)) {
      return null
    }

    return (
      <div className="panel">
        <div className="panel__title">
          Редактировать шаг
        </div>
        <div className="input-group mb-3">
          <input type="text" className="form-control" placeholder="Название" aria-label="Title" aria-describedby="basic-addon1" defaultValue={ step.title } onChange={ this.changeTitle.bind(this) } ref={ this.titleInput } />
        </div>
        <div className="panel__btns">
          <button className="btn btn-outline-primary" onClick={ this.saveChanges.bind(this) }>Сохранить</button>
          <button className="btn btn-outline-secondary" onClick={ () => history.push('/') }>Отменить</button>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StepEdit)