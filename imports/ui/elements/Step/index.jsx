import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import classNames from 'classnames'
import { connect } from 'react-redux'
import { _ } from 'lodash'
import Dragula from 'react-dragula'
import { withRouter } from 'react-router-dom'

import 'dragula/dist/dragula.css'

import { formatTime } from '../../../lib/time'

import StepElement from '../StepElement/index'

import './styles.less'

import { addElement } from '../../../redux/actions'

// function mapStateToProps(state) {  
//   return { ...state }
// }

function mapDispatchToProps(dispatch) {  
  return {
    addElement: newElement => dispatch(addElement(newElement)),
  }
}

class Step extends Component {
  static propTypes = {
    step: PropTypes.shape({
      index: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      elements: PropTypes.array.isRequired,
    }).isRequired,
    stageIndex: PropTypes.number.isRequired,
  }

  static defaultProps = {
    // propName: 'value',
  }

  constructor() {
    super()

    this.elementsContainer = React.createRef()
  }

  componentDidMount() {
    Dragula([this.elementsContainer.current], {
      moves(el, container, target) {
        const parent = target.closest('[data-role="dragula/container"]')
        return parent.dataset.id === 'elements'
      }
    })
  }

  addElement() {
    const { step, stageIndex, addElement } = this.props
    const { elements } = step

    let maxIndexElement = _.maxBy(elements, 'index')
    let maxIndex

    if (_.isUndefined(maxIndexElement)) {
      maxIndex = 0
    } else {
      maxIndex = maxIndexElement.index
    }

    const index = maxIndex + 1
    addElement({
      stageIndex,
      stepIndex: step.index,
      content: {
        index,
        title: `#${ index }`,
        time: '00:00',
        responsible: '',
      }
    })
  }

  render() {
    const { step, stageIndex, history } = this.props
    const { elements } = step

    let time = 0

    for (const element of elements) {
      const timeArr = element.time.split(':')
      time += +timeArr[0] * 60 + +timeArr[1]
    }

    return (
      <div className="step__cont">
        <div className="step__title-cont">
          <div onClick={ () => history.push(`/${ stageIndex }/${ step.index }`) }>{ step.title }</div>
          <div>{ formatTime(time) }</div>
        </div>
        <div className="step__grid" data-role="dragula/container" data-id="elements" ref={ this.elementsContainer }>
          { elements.map(element => (
            <StepElement key={ element.index } element={ element } stepIndex={ step.index } stageIndex={ stageIndex }/>
          )) }
        </div>
        { elements.length < 3 && (
          <div className="step__add-cont">
            <button className="btn step__add-element" onClick={ this.addElement.bind(this) }>+</button>
          </div>
        )}
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(withRouter(Step))