import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { _ } from 'lodash'

import InputMask from 'react-input-mask'

import './styles.less'
import '../../styles/panel.less'

import { changeElement } from '../../../redux/actions'

function mapStateToProps(state) {  
  return { ...state.stages }
}

function mapDispatchToProps(dispatch) {  
  return {
    changeElement: newProps => dispatch(changeElement(newProps)),
  }
}

class StepElementEdit extends Component {
  constructor() {
    super()

    this.state = {
      title: null,
      responsible: null,
      time: null,
    }

    this.getElement = this.getElement.bind(this)
    this.titleInput = React.createRef()
    this.responsibleInput = React.createRef()
    this.timeInput = React.createRef()
  }

  changeTitle(event) {
    this.setState({
      title: event.target.value,
    })
  }

  changeResponsible(event) {
    this.setState({
      responsible: event.target.value,
    })
  }

  changeTime(event) {
    this.setState({
      time: event.target.value,
    })
  }

  saveChanges() {
    const { title, responsible, time } = this.state
    const { history } = this.props

    const newProps = {}
    let isChanged = false

    if (!_.isNull(title)) {
      newProps.title = title
      isChanged = true
    }
    if (!_.isNull(responsible)) {
      newProps.responsible = responsible
      isChanged = true
    }
    if (!_.isNull(time) && /([0-9][0-9]:[0-9][0-9])$/.test(time)) {
      newProps.time = time
      isChanged = true
    }

    if (isChanged) {
      const { match, changeElement } = this.props
      const stageIndex = +match.params.stageIndex
      const stepIndex = +match.params.stepIndex
      const elementIndex = +match.params.elementIndex
  
      changeElement({
        stageIndex,
        stepIndex,
        elementIndex,
        content: newProps,
      })
    }
    
    history.push('/')
  }

  componentDidUpdate(prevProps) {
    const { stages, match } = this.props
    const { match: prevMatch } = prevProps

    const stageIndex = +match.params.stageIndex
    const stepIndex = +match.params.stepIndex
    const elementIndex = +match.params.elementIndex

    if (+prevMatch.params.stageIndex !== stageIndex || +prevMatch.params.stepIndex !== stepIndex || +prevMatch.params.elementIndex !== elementIndex) {
      const element = this.getElement()

      if (_.isUndefined(element)) {
        return
      }
      
      setTimeout(() => {
        this.titleInput.current.value = element.title
        this.responsibleInput.current.value = element.responsible
        this.timeInput.current.value = element.time
      }, 0) // time input fix
    }
  }

  getElement() {
    const { stages, match } = this.props
    const stageIndex = +match.params.stageIndex
    const stepIndex = +match.params.stepIndex
    const elementIndex = +match.params.elementIndex

    const stage = _.find(stages, ['index', stageIndex])

    if (_.isUndefined(stage)) {
      return
    }

    const step = _.find(stage.steps, ['index', stepIndex])

    if (_.isUndefined(step)) {
      return
    }

    return _.find(step.elements, ['index', elementIndex])
  }

  render() {
    const { history } = this.props

    const element = this.getElement()

    if (_.isUndefined(element)) {
      return null
    }

    return (
      <div className="panel">
        <div className="panel__title">
          Редактировать элемент
        </div>
        <div className="input-group mb-3">
          <input type="text" className="form-control" placeholder="Название" aria-label="Title" aria-describedby="basic-addon1" defaultValue={ element.title } onChange={ this.changeTitle.bind(this) } ref={ this.titleInput } />
        </div>
        <div className="input-group mb-3">
          <input type="text" className="form-control" placeholder="Ответственный" aria-label="Responsible" aria-describedby="basic-addon1" defaultValue={ element.responsible } onChange={ this.changeResponsible.bind(this) } ref={ this.responsibleInput } />
        </div>
        <div className="input-group mb-3">
          <InputMask mask="99:99" defaultValue={ element.time } onChange={ this.changeTime.bind(this) }>
            { (inputProps) => <input type="text" className="form-control" placeholder="Время" aria-label="Time" aria-describedby="basic-addon1" ref={ this.timeInput } { ...inputProps } /> }
          </InputMask>
        </div>
        <div className="panel__btns">
          <button className="btn btn-outline-primary" onClick={ this.saveChanges.bind(this) }>Сохранить</button>
          <button className="btn btn-outline-secondary" onClick={ () => history.push('/') }>Отменить</button>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StepElementEdit)