import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { _ } from 'lodash'

import './styles.less'
import '../../styles/panel.less'

import { changeStage } from '../../../redux/actions'

function mapStateToProps(state) {  
  return { ...state.stages }
}

function mapDispatchToProps(dispatch) {  
  return {
    changeStage: newProps => dispatch(changeStage(newProps)),
  }
}

class StageEdit extends Component {
  constructor() {
    super()

    this.state = {
      title: null,
    }

    this.getStage = this.getStage.bind(this)
    this.titleInput = React.createRef()
  }

  changeTitle(event) {
    this.setState({
      title: event.target.value,
    })
  }

  saveChanges() {
    const { title } = this.state
    const { history } = this.props

    if (!_.isNull(title)) {
      const { match, changeStage } = this.props
      const stageIndex = +match.params.stageIndex

      changeStage({
        stageIndex,
        content: {
          title,
        },
      })
    }
    
    history.push('/')
  }

  componentDidUpdate(prevProps) {
    const { match } = this.props
    const { match: prevMatch } = prevProps

    const stageIndex = +match.params.stageIndex

    if (+prevMatch.params.stageIndex !== stageIndex) {
      const stage = this.getStage()
  
      if (_.isUndefined(stage)) {
        return
      }
  
      this.titleInput.current.value = stage.title
    }
  }

  getStage() {
    const { stages, match, history } = this.props
    const stageIndex = +match.params.stageIndex

    return _.find(stages, ['index', stageIndex])
  }

  render() {
    const { history } = this.props

    const stage = this.getStage()

    if (_.isUndefined(stage)) {
      return null
    }

    return (
      <div className="panel">
        <div className="panel__title">
          Редактировать этап
        </div>
        <div className="input-group mb-3">
          <input type="text" className="form-control" placeholder="Название" aria-label="Title" aria-describedby="basic-addon1" defaultValue={ stage.title } ref={ this.titleInput } onChange={ this.changeTitle.bind(this) } />
        </div>
        <div className="panel__btns">
          <button className="btn btn-outline-primary" onClick={ this.saveChanges.bind(this) }>Сохранить</button>
          <button className="btn btn-outline-secondary" onClick={ () => history.push('/') }>Отменить</button>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StageEdit)