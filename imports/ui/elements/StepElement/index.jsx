import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'

import './styles.less'

class StepElement extends Component {
  static propTypes = {
    element: PropTypes.shape({
      index: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      time: PropTypes.string.isRequired,
      responsible: PropTypes.string.isRequired,
    }).isRequired,
    stepIndex: PropTypes.number.isRequired,
    stageIndex: PropTypes.number.isRequired,
  }

  static defaultProps = {
    // propName: 'value',
  }

  render() {
    const { element, history, stageIndex, stepIndex } = this.props

    return (
      <div className="step-element" onClick={ () =>  history.push(`/${ stageIndex }/${ stepIndex }/${ element.index }`) }>
        <img src="/list.png" className="step-element__ico"/>
        <span className="step-element__title">{ element.title }</span>
      </div>
    )
  }
}

export default withRouter(StepElement)