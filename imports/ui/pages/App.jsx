import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'

import 'dragula/dist/dragula.css'

import Main from '../pages/Main/index'

class App extends Component {
  render() {
    return (
      <Router>
        <Switch location={ location }>
          <Route path="/" component={ Main } />
          {/* <Redirect to="/" /> */}
        </Switch>
      </Router>
    )
  }
}

export default App