import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { _ } from 'lodash'
import Dragula from 'react-dragula'

import 'dragula/dist/dragula.css'

import { addStage } from '../../../redux/actions'

import Stage from '../../elements/Stage/index'
import StageEdit from '../../elements/StageEdit/index'
import StepEdit from '../../elements/StepEdit/index'
import StepElementEdit from '../../elements/StepElementEdit/index'

function mapStateToProps(state) {  
  return { ...state.stages }
}

function mapDispatchToProps(dispatch) {  
  return {
    addStage: newStage => dispatch(addStage(newStage)),
  }
}

class Main extends Component {
  constructor() {
    super()

    this.stagesContainer = React.createRef()
  }

  componentDidMount() {
    Dragula([this.stagesContainer.current], {
      moves(el, container, target) {
        const parent = target.closest('[data-role="dragula/container"]')
        return parent.dataset.id === 'stages'
      }
    })
  }

  addStage() {
    const { stages, addStage } = this.props

    let maxIndexStage = _.maxBy(stages, 'index')
    let maxIndex

    if (_.isUndefined(maxIndexStage)) {
      maxIndex = 0
    } else {
      maxIndex = maxIndexStage.index
    }

    const index = maxIndex + 1
    const newStage = {
      title: `Этап №${ index }`,
      index,
      steps: [],
    }

    addStage(newStage)
  }

  render() {
    const { stages } = this.props

    return (
      <div>
        <div className="container">
          <div className="row" style={ {
            marginTop: '3rem',
          } }>
            <div className="col">
              <button className="btn btn-primary" onClick={ this.addStage.bind(this) }>Добавить этап</button>
            </div>
          </div>
        </div>
        <div className="container" data-role="dragula/container" data-id="stages" ref={ this.stagesContainer }>
          { stages.map(stage => (
            <Stage key={ stage.index } stage={ stage } />
          )) }
        </div>
        <Route exact path="/:stageIndex" component={ StageEdit } />
        <Route exact path="/:stageIndex/:stepIndex" component={ StepEdit } />
        <Route exact path="/:stageIndex/:stepIndex/:elementIndex" component={ StepElementEdit } />
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)