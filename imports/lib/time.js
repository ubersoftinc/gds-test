import { _ } from 'lodash'

export function formatTime(time) {
  let hours = _.floor(time / 60)
  let minutes = time - hours * 60
  
  if (hours < 10) {
    hours = `0${ hours }`
  }
  
  if (minutes < 10) {
    minutes = `0${ minutes }`
  }

  return `${ hours }:${ minutes }`
}