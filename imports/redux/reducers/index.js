import { combineReducers } from 'redux'

import stagesReducers from './stages'

export default combineReducers({ stages: stagesReducers })