const stagesReducers = (state = { stages: [] }, action) => {
  let stages
  switch (action.type) {
    case 'ADD_STAGE':
      stages = state.stages.slice(0)
      stages.push(action.payload)
      return { ...state, stages }
    case 'CHANGE_STAGE':
      stages = state.stages.map(stage => {
        if (stage.index !== action.payload.stageIndex) {
          return stage
        }
        
        return {
          ...stage,
          ...action.payload.content,
        } 
      })
      return { ...state, stages }
    case 'ADD_STEP':
      stages = state.stages.map(stage => {
        if (stage.index !== action.payload.stageIndex) {
          return stage
        }
        
        const steps = stage.steps.slice(0)
        steps.push(action.payload.content)

        return {
          ...stage,
          steps
        } 
      })
      return { ...state, stages }
    case 'CHANGE_STEP':
      stages = state.stages.map(stage => {
        if (stage.index !== action.payload.stageIndex) {
          return stage
        }

        const steps = stage.steps.map(step => {
          if (step.index !== action.payload.stepIndex) {
            return step
          }

          return {
            ...step,
            ...action.payload.content,
          }
        })

        return {
          ...stage,
          steps
        }
      })
      return { ...state, stages }
    case 'ADD_ELEMENT':
      stages = state.stages.map(stage => {
        if (stage.index !== action.payload.stageIndex) {
          return stage
        }
        
        const steps = stage.steps.map(step => {
          if (step.index !== action.payload.stepIndex) {
            return step
          }

          const elements = step.elements.slice(0)
          elements.push(action.payload.content)

          return {
            ...step,
            elements
          }
        })

        return {
          ...stage,
          steps
        }
      })
      return { ...state, stages }
    case 'CHANGE_ELEMENT':
      stages = state.stages.map(stage => {
        if (stage.index !== action.payload.stageIndex) {
          return stage
        }

        const steps = stage.steps.map(step => {
          if (step.index !== action.payload.stepIndex) {
            return step
          }

          const elements = step.elements.map(element => {
            if (element.index !== action.payload.elementIndex) {
              return element
            }

            return {
              ...element,
              ...action.payload.content,
            }
          })

          return {
            ...step,
            elements,
          }
        })

        return {
          ...stage,
          steps
        }
      })
      return { ...state, stages }
    default:
      return state
  }
}

export default stagesReducers