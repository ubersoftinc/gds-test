export const addStage = newStage => ({ type: 'ADD_STAGE', payload: newStage })
export const changeStage = newProps => ({ type: 'CHANGE_STAGE', payload: newProps })

export const addStep = newStep => ({ type: 'ADD_STEP', payload: newStep })
export const changeStep = newProps => ({ type: 'CHANGE_STEP', payload: newProps })

export const addElement = newElement => ({ type: 'ADD_ELEMENT', payload: newElement })
export const changeElement = newProps => ({ type: 'CHANGE_ELEMENT', payload: newProps })
